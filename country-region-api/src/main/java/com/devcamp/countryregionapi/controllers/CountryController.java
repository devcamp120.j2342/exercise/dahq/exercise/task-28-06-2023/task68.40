package com.devcamp.countryregionapi.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.countryregionapi.models.Country;
import com.devcamp.countryregionapi.reponsitorys.CountryRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CountryController {
    @Autowired
    CountryRepository countryRepository;

    @GetMapping("/country/all")
    public ResponseEntity<List<Country>> getAllCountry() {

        return new ResponseEntity<>(countryRepository.findAll(), HttpStatus.OK);
    }

    @PostMapping("/country/create")
    public ResponseEntity<Country> createCountry(@RequestBody Country pCountry) {
        try {
            Country vCountry = new Country();
            // vCountry.setRegionsTotal(10);
            vCountry.setCountryName(pCountry.getCountryName());
            vCountry.setCountryCode(pCountry.getCountryCode());
            vCountry.setRegions(pCountry.getRegions());

            // Country cCountry = countryRepository.save(pCountry);
            return new ResponseEntity<>(countryRepository.save(vCountry), HttpStatus.CREATED);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/country/update/{id}")
    public ResponseEntity<Country> updateCountry(@RequestBody Country pCountry, @PathVariable("id") long id) {
        Optional<Country> couOptional = countryRepository.findById(id);
        if (couOptional.isPresent()) {
            try {
                Country cCountry = couOptional.get();
                cCountry.setCountryCode(pCountry.getCountryCode());
                cCountry.setCountryName(pCountry.getCountryName());
                cCountry.setRegions(pCountry.getRegions());
                return new ResponseEntity<>(countryRepository.save(cCountry), HttpStatus.OK);

            } catch (Exception e) {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<Country>(HttpStatus.NOT_FOUND);
        }

    }

    @DeleteMapping("/country/delete/{id}")
    public ResponseEntity<Country> deleteCountry(@PathVariable("id") long id) {
        try {
            countryRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/country/details/{id}")
    public Country getCountryById(@PathVariable("id") long id) {
        Optional<Country> cCountry = countryRepository.findById(id);
        return cCountry.get();
    }

}
