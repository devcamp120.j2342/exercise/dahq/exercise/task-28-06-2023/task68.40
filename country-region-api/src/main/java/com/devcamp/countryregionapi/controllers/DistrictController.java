package com.devcamp.countryregionapi.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.countryregionapi.models.District;
import com.devcamp.countryregionapi.models.Region;
import com.devcamp.countryregionapi.reponsitorys.DistrictRepository;
import com.devcamp.countryregionapi.reponsitorys.RegionRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class DistrictController {
    @Autowired
    DistrictRepository districtRepository;
    @Autowired
    RegionRepository regionRepository;

    @GetMapping("/district/all")
    public List<District> getAllDistrict() {
        return districtRepository.findAll();
    }

    @PostMapping("/district/create/{id}")
    public ResponseEntity<District> createDistrict(@RequestBody District pDistrict, @PathVariable("id") long id) {
        Optional<Region> cRegion = regionRepository.findById(id);
        if (cRegion.isPresent()) {
            try {
                District newDistrict = new District();
                newDistrict.setName(pDistrict.getName());
                newDistrict.setPrefix(pDistrict.getPrefix());
                newDistrict.setRegion(cRegion.get());
                return new ResponseEntity<District>(districtRepository.save(newDistrict), HttpStatus.CREATED);
            } catch (Exception e) {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @PutMapping("/district/update/{id}")
    public ResponseEntity<District> updateDistrict(@RequestBody District pDistrict, @PathVariable("id") long id) {
        Optional<District> cDistrict = districtRepository.findById(id);
        if (cDistrict.isPresent()) {
            try {
                cDistrict.get().setName(pDistrict.getName());
                cDistrict.get().setPrefix(pDistrict.getPrefix());
                cDistrict.get().setRegion(pDistrict.getRegion());
                return new ResponseEntity<District>(districtRepository.save(cDistrict.get()), HttpStatus.OK);

            } catch (Exception e) {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @DeleteMapping("/district/delete/{id}")
    public ResponseEntity<District> deleteDistrict(@PathVariable("id") long id) {
        districtRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/district/details/{id}")
    public District getDistrictById(@PathVariable("id") long id) {
        return districtRepository.findById(id).get();
    }

}
