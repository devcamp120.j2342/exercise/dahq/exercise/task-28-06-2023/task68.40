package com.devcamp.countryregionapi.reponsitorys;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.countryregionapi.models.District;

public interface DistrictRepository extends JpaRepository<District, Long> {

}
