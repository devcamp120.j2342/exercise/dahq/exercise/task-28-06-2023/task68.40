package com.devcamp.countryregionapi.reponsitorys;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.countryregionapi.models.Country;

public interface CountryRepository extends JpaRepository<Country, Long> {
    Country findByCountryCode(String countryCode);

}
